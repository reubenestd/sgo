import argparse
import gitlab
import base64
import ruamel.yaml as yaml


class IndentyRoundTripDumper(yaml.RoundTripDumper):
    def increase_indent(self, flow=False, indentless=False, **kwargs):
        return super(IndentyRoundTripDumper, self).increase_indent(flow, False)


GITLAB = "GITLAB"
GITHUB = "GITHUB"

parser = argparse.ArgumentParser(description='Update a k8s config repo based on a git tag corresponding to a docker '
                                             'image tag')
parser.add_argument('--config', type=str,
                    required=True,
                    help='The config repo to update')
parser.add_argument('--config-branch', type=str,
                    required=False,
                    default='master',
                    help='The config repo to update')
parser.add_argument('--config-file', type=str, required=True, help='The config file to update')
parser.add_argument('--yaml-path', type=str, required=True, help='The path in the yaml to update')

parser.add_argument('--tag', type=str, required=True, help='The tag to update to')
parser.add_argument('--image', type=str, required=True, help='The image to reference')

parser.add_argument('--git-host', type=str, required=False, default=GITLAB,
                    help='The git provider you use (e.g. GitLab, GitHub etc.)')
parser.add_argument('--git-token', type=str, required=True, help='The access token for your git host.')
parser.add_argument('--project-name', type=str, required=True,
                    help='The name of the project to which this image belongs.')


class GitLab:
    def __init__(self, token: str, repo: str):
        self.gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
        self.repo = self.gl.projects.get(repo)

    def get_file_content(self, file_id: str):
        file_info = self.repo.repository_blob(sha=file_id)
        content = base64.b64decode(file_info['content'])
        return content

    def get_file(self, path: str, branch: str):
        split_path = path.split('/')
        directory = "/".join(split_path[:-1])
        filename = split_path[-1]
        items = self.repo.repository_tree(path=directory, ref=branch)
        file = next(i for i in items if i["name"] == filename)
        return self.get_file_content(file_id=file['id']).decode('utf-8')

    def commit_file(self, path: str, contents: str, tag: str, project_name: str, start_branch: str):
        branch = f'{project_name}-{tag}'
        data = {
            'branch': branch,
            'commit_message': f"Set image tag to {tag} for {project_name}",
            'start_branch': start_branch,
            'actions': [
                {
                    'action': 'update',
                    'file_path': path,
                    'content': contents,
                },
            ]
        }
        self.repo.commits.create(data)
        self.create_pr(tag=tag, project_name=project_name, branch_from=branch, branch_to=start_branch)

    def create_pr(self, tag: str, project_name: str, branch_from: str, branch_to: str):
        self.repo.mergerequests.create({'source_branch': branch_from,
                                        'target_branch': branch_to,
                                        'title': f'Increment {project_name} image to {tag}',
                                        'labels': ['automated', project_name]})


GIT_HOST_MAP = {
    GITLAB: GitLab,
}


def _indx_or_key(k: str):
    return k if not k.isdigit() else int(k)


def modify_yaml_entry_by_path(file: str, path: str, new_value: str):
    yaml_tree = yaml.load(file, Loader=yaml.RoundTripLoader)
    split_path = path.split('.')
    our_entry = None
    for k in split_path[:-1]:
        our_entry = yaml_tree[k] if our_entry is None else our_entry[_indx_or_key(k)]
    our_entry[_indx_or_key(split_path[-1])] = new_value
    return yaml.dump(yaml_tree, Dumper=IndentyRoundTripDumper)


def main():
    args = parser.parse_args()
    host = args.git_host
    try:
        gh_inst = GIT_HOST_MAP[host](token=args.git_token, repo=args.config)
    except KeyError:
        raise NotImplemented(f"Sorry {host} is not supported at the moment")
    original_file = gh_inst.get_file(path=args.config_file, branch=args.config_branch)
    updated_file = modify_yaml_entry_by_path(original_file, path=args.yaml_path, new_value=f"{args.image}:{args.tag}")
    gh_inst.commit_file(path=args.config_file, contents=updated_file, tag=args.tag, project_name=args.project_name,
                        start_branch=args.config_branch)


if __name__ == '__main__':
    main()
