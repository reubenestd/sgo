FROM python:3.7
ADD requirements.txt /script/
WORKDIR /script/
RUN pip3 install -r requirements.txt
COPY . /script/
ENTRYPOINT ["python3", "sgo.py"]